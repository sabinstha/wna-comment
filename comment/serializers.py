from rest_framework import serializers
from comment.models import Comment


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ('id', 'user', 'comment', 'created_date', 'parent', 'replies')



    def get_fields(self):
        fields = super(CommentSerializer, self).get_fields()
        fields['replies'] = CommentSerializer(many=True)
        fields['created_date'] = serializers.DateTimeField(format="%H:%M, %d %h %a, %Y ")
        return fields

