from django import forms


class CommentForm(forms.Form):
    user = forms.CharField()
    comment = forms.CharField()


