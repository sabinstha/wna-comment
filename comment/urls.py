from django.urls import path
from comment.api import *

urlpatterns = [
    path('comments', getData, name='comments'),
    path('comment/add/', addComment, name='add_comment'),
]