from django.db import models
from django.utils.timezone import now


# Create your models here.
class Comment(models.Model):
    class Meta:
        db_table = 'comments'

    user = models.CharField(max_length=255)
    post = models.IntegerField(null=True, default=None)
    parent = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='replies')
    comment = models.TextField()
    created_date = models.DateTimeField(default=now)
    modified_date = models.DateTimeField(default=now)

    def FORMAT(self):
        from django.utils.timesince import timesince
        return timesince(self.created_date)
