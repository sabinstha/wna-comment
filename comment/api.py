from rest_framework.response import Response
from rest_framework.decorators import api_view
from comment.models import Comment
from comment.serializers import CommentSerializer
from django.utils.timezone import now
from comment.forms import CommentForm


@api_view(['GET'])
def getData(request):
    post_id = request.GET.get('post_id')
    comments = Comment.objects.filter(post=post_id, parent=None).order_by('-id')
    serializer = CommentSerializer(comments, many=True)
    content = {
        'status': 200,
        'success': True,
        'message': 'Comments',
        'data': serializer.data
    }
    return Response(content)


@api_view(['POST'])
def addComment(request):
    comment = Comment(user=request.data.get('user'),
                      comment=request.data.get('comment'),
                      post=1,
                      created_date=now(),
                      modified_date=now(),
                      parent_id=request.data.get('parent_id')
                      )
    form_data = CommentForm(request.POST)
    if form_data.is_valid():
        serializer = CommentSerializer(comment, many=False)
        try:
            comment.save()
            content = {
                'status': 200,
                'success': True,
                'message': 'Comment added successfully',
                'data': serializer.data
            }
        except:
            content = {
                'status': 400,
                'success': False,
                'message': 'Insertion Error',
                'data': serializer.data
            }
    else:
        print(form_data.errors)
        content = {
            'status': 400,
            'success': False,
            'message': 'Validation error both the input fields are required',
            'data': []
        }
    return Response(content)
